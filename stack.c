#include <stdio.h>
#include <stdlib.h>
#include "stack.h"



int value = 0;

stack createStack(int maxNumber){
    stack s = malloc(sizeof(stack));
    s->stack = malloc(sizeof(int)*maxNumber);
    s->number = 0;
    s->headIndex = 0;
    s->maxValue = maxNumber;
    return s;
}

int putValue(stack s, int value){

    if(s->number++ > s->maxValue){
        return EXIT_FAILURE;
    }
    s->number++;

    s->headIndex = s->headIndex+1 % s->maxValue;
    s->stack[s->headIndex] = value;

    return EXIT_SUCCESS;
}

int getValue(stack s){
    if(s->number == 0){
        exit(1);
    }
    s->number--;

    int value = s->stack[s->headIndex];
    s->headIndex = s->headIndex-1 % s->maxValue;
    return value;
}

int isStackEmpty(stack s){
    if(s->number == 0){
        return EXIT_SUCCESS;
    }
    else{
        return EXIT_FAILURE;
    }
}

int freeStack(stack s){
    free(s->stack);
    free(s);
    return EXIT_SUCCESS;
}
