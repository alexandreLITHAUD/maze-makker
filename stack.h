#ifndef STACK_H
#define STACK_H

typedef struct{
    int* stack;
    int number;
    int headIndex;
    int maxValue;
} *stack;

stack createStack(int maxNumber);

int putValue(stack s, int value);

int getValue(stack s);

int isStackEmpty(stack s);

int freeStack(stack s);

#endif