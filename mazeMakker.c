#include <SDL2/SDL.h>
#include <math.h>
#include <time.h>
#include "stack.h"

#define WINDOW_WIDTH 800
#define WINDOW_HEIGH 600
#define BANNER_PANELS_WIDTH 80

#define MAZE 510

int mazeDefinition(int **mazeArray, int width, int height)
{
    for (int i = 0; i < width; i++)
    {
        mazeArray[i] = (int *)malloc(height * sizeof(int));
        for (int j = 0; j < height; j++)
        {
            mazeArray[i][j] = SDL_MapRGB(SDL_AllocFormat(SDL_PIXELFORMAT_RGBA8888), 0, 0, 0);
        }
    }

    return EXIT_SUCCESS;
}

int mazeCreation(int **mazeArray, int width, int height)
{
    // One on two
    for (int i = 0; i < width; i += 2)
    {
        for (int j = 0; j < width; j += 2)
        {
            mazeArray[i][j] = SDL_MapRGB(SDL_AllocFormat(SDL_PIXELFORMAT_RGBA8888), 255, 255, 255);
        }
    }

    // Create Maze
    stack s = createStack(width*height);
    //ALGO


    freeStack(s);
    return EXIT_SUCCESS;
}

int createMaze(int x, int y, int width, int height, SDL_Renderer *renderer)
{

    if (width < MAZE || height < MAZE)
    {
        perror("Pane is too small");
        return -1;
    }

    int widthDiff = floor((width - MAZE) / 2);
    int heighDiff = floor((height - MAZE) / 2);

    // CREATE MAZE SQUARE FIRST
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 128);

    widthDiff += x;
    heighDiff += y;

    int** mazeArray = (int **)malloc(MAZE * sizeof(int *));

    mazeDefinition(mazeArray, MAZE, MAZE);

    mazeCreation(mazeArray, MAZE, MAZE);

    for (int i = 0; i < MAZE; i++)
    {
        for (int j = 0; j < MAZE; j++)
        {
            SDL_SetRenderDrawColor(renderer,
                                   (mazeArray[i][j] >> 16) & 0xFF,
                                   (mazeArray[i][j] >> 8) & 0xFF,
                                   mazeArray[i][j] & 0xFF, 255);
            SDL_RenderDrawPoint(renderer, widthDiff + i, heighDiff + j);
        }
    }

    // SDL_Rect mazePane = {widthDiff, heighDiff, MAZE, MAZE};
    // SDL_RenderFillRect(renderer, &mazePane);

    return 0;
}

int main(int argc, char *argv[])
{

    srand(time(NULL));

    // Initialize SDL
    SDL_Init(SDL_INIT_VIDEO);

    // Create a window
    SDL_Window *window = SDL_CreateWindow("Game Panel", SDL_WINDOWPOS_CENTERED,
                                          SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGH, 0);

    // Create a renderer
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    // Main loop
    int isRunning = 1;
    while (isRunning)
    {
        // Handle events
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT)
            {
                isRunning = 0;
            }
        }

        // Clear the renderer
        SDL_RenderClear(renderer);

        // ALGO PANE (BLUE)
        SDL_SetRenderDrawColor(renderer, 0, 0, 255, 128);
        SDL_Rect algoPane = {0, 0, WINDOW_WIDTH, BANNER_PANELS_WIDTH};
        SDL_RenderFillRect(renderer, &algoPane);

        // PARAMETER PANE (GREEN)
        SDL_SetRenderDrawColor(renderer, 0, 255, 0, 128);
        SDL_Rect parameterPane = {WINDOW_WIDTH - BANNER_PANELS_WIDTH, BANNER_PANELS_WIDTH, BANNER_PANELS_WIDTH, WINDOW_HEIGH - BANNER_PANELS_WIDTH};
        SDL_RenderFillRect(renderer, &parameterPane);

        // MAZE PANE (RED)
        SDL_SetRenderDrawColor(renderer, 255, 0, 0, 128);
        SDL_Rect mazePane = {0, BANNER_PANELS_WIDTH, WINDOW_WIDTH - BANNER_PANELS_WIDTH, WINDOW_HEIGH - BANNER_PANELS_WIDTH};
        SDL_RenderFillRect(renderer, &mazePane);

        if (createMaze(0, BANNER_PANELS_WIDTH, WINDOW_WIDTH - BANNER_PANELS_WIDTH, WINDOW_HEIGH - BANNER_PANELS_WIDTH, renderer))
        {
            return 1;
        }

        // Background
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

        // Update the renderer
        SDL_RenderPresent(renderer);
    }

    // Cleanup
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}