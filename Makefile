#TODO
CC = gcc
LFLAG = -lSDL2 -lm
DFLAG = -g
NAME = maze

SRC := $(wildcard *.c)
OBJ := $(SRC:%.c=%.o)
DEP := $(OBJ:%.o=%.d)

.PHONY: all exe clean

all: $(NAME) 
exe: $(NAME) launch clean

$(NAME): $(OBJ)
	$(CC) $(DFLAG) $^ -o $@ $(LFLAG)

%.o: %.c
	$(CC) $(DFLAG) -MMD -c $< -o $@ $(LFLAG)

-include $(DEPS)

launch:
	./$(NAME)

clean:
	rm -f mazeMakker.o $(NAME) $(DEP) $(OBJ)
